function SceneToWebsocketsAdapter(webSocketsClient, playerName) {

  this.DEBUG = true;
  this.playerName = playerName;
  this.webSocketsClient = webSocketsClient;
  this.sceneObjects = new List();

  this.enter = function() {
    var command = new Command(CommandType.EnterTheGame, "{\"name\" : \"" + this.playerName + "\"}");
    this.webSocketsClient.sendData(command.serialized());
  };

  this.getObjects = function() {
    var data = "{}";
    var command = new Command(CommandType.GetObjects, data);
    this.webSocketsClient.sendData(command.serialized());
  };

  this.sendObject = function(object) {

    var x = object.position.x;
    var y = object.position.y;
    var z = object.position.z;

    var rX = object.rotation.x;
    var rY = object.rotation.y;
    var rZ = object.rotation.z;

    var sX = object.scale.x;
    var sY = object.scale.y;
    var sZ = object.scale.z;

    data = "{\
        \"name\": \"" + this.playerName + "\",\
        \"x\"  : " + x + ",\
        \"y\"  : " + y + ",\
        \"z\"  : " + z + ",\
        \"rX\" : " + rX + ",\
        \"rY\" : " + rY + ",\
        \"rZ\" : " + rZ + ",\
        \"sX\" : " + sX + ",\
        \"sY\" : " + sY + ",\
        \"sZ\" : " + sZ + "
    }";

    var command = new Command(CommandType.SendObject, data);
    this.webSocketsClient.sendData(command.serialized());
  };

  this.handleScene = function(scene) {
    print("handleScene");
  };

  this.webSocketsClientDidReceiveData = function(client, data) {
    var dictionary = eval(data);
    var callResult = new CallResult(dictionary);

    print("code:" + dictionary.code);
    print("description" + dictionary.description);

    if (callResult.code == CallResultCode.Success) {
      if (this.DEBUG) {
        print(callResult.description);
      }
    }
    else if (callResult.code == CallResultCode.Fail) {
      // TODO: GUI without exit
      print(callResult.description);
      exit(1);
    }
    else if (callResult.code == CallResultCode.SceneObjects) {
      if (this.DEBUG) {
        print("callResult.objects.length: " + callResult.objects.length);
      }
      var self = this;
      callResult.objects.forEach(function(serverObject) {
        var objectToUpdate = self.sceneObjects.getFirstMatch(function(item) {
          return item == serverObject.name;
        });
        if (objectToUpdate == null) {
          self.addSceneObject(item);
        }
        else {
          self.updateSceneObject(item);
        };
      });
    }
  };

  this.addSceneObject = function(object) {
    this.sceneObjects.push(object.name);
    var sceneObject = createObject();
    sceneObject.name = object.name;
    sceneObject.modelPath = "com.demensdeum.flamesteelengine.cube.fsglmodel";
    sceneObject.position.x = object.x;
    sceneObject.position.y = object.y;
    sceneObject.position.z = object.z;
    addObject(sceneObject);
    if (this.DEBUG) {
      print("addSceneObject: " + object.name);
    }
  };

  this.updateSceneObject = function(object) {
    if (this.DEBUG) {
      print("updateSceneObject: " + object.name);
    }
    var sceneObjectToUpdate = getObject(object.name);
    sceneObjectToUpdate.position.x = object.x;
    sceneObjectToUpdate.position.y = object.y;
    sceneObjectToUpdate.position.z = object.z;
    updateObject(sceneObjectToUpdate);
  };

  this.exit = function() {
    var command = new Command(CommandType.ExitTheGame, "{}");
    this.webSocketsClient.sendData(command.serialized());
  };

};
