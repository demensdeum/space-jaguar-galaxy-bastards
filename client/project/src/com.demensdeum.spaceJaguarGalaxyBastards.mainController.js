function MainController(address) {
  this.address = address;

  this.mode = "Menu";
  this.entered = false;
  this.sceneToWebsocketsAdapter = null;

  this.initializeIfNeeded = function() {
    if (this.initialized != true) {
      addDefaultCamera();

      var camera = getObject("camera");
      this.freeViewManager = new FreeViewManager(camera, 0.1);

      this.webSocketsClient = new WebSocketsClient();
      this.webSocketsClient.connect(this.address, this);

      this.showMenu();
      this.initialized = true;
    }
  };

  this.showMenu = function() {
    this.menuPresented = true;
    addButton("Send object mode", "SendObject");
    addButton("Read objects mode", "GetObjects");
    addButton("Exit", "Exit");
  };

  this.handleMenu = function() {
      if (isButtonPressed("SendObject")) {
        this.mode = "SendObject";
        this.sceneToWebsocketsAdapter = new SceneToWebsocketsAdapter(this.webSocketsClient, "ObjectSender");
        enablePointerLock();
        destroyWindow();
      }
      else if (isButtonPressed("GetObjects")) {
        this.mode = "GetObjects";
        this.sceneToWebsocketsAdapter = new SceneToWebsocketsAdapter(this.webSocketsClient, "SceneReader");
        enablePointerLock();
        destroyWindow();
      }
      else if ("Exit") {
        print("Bye-Bye!")
        exit(0);
      }
  };

  this.handleMode = function() {

    if (this.mode == "Menu") {
      this.handleMenu();
    }
    else if (this.mode == "SendObject") {
      var camera = getObject("camera");
      if (this.entered != true) {
        this.entered = true;
        this.sceneToWebsocketsAdapter.enter();
      }
      this.sceneToWebsocketsAdapter.sendObject(camera);
    }
    else if (this.mode == "GetObjects") {
      if (this.entered != true) {
        this.entered = true;
        this.sceneToWebsocketsAdapter.enter();
      }
      this.sceneToWebsocketsAdapter.getObjects();
    }

  };

  this.step = function() {
    this.initializeIfNeeded();
    this.freeViewManager.step();
    this.handleMode();
  };

  this.webSocketsClientDidReceiveData = function(client, data) {
    if (this.sceneToWebsocketsAdapter != null) {
      this.sceneToWebsocketsAdapter.webSocketsClientDidReceiveData(client, data);
    }
  };
};
