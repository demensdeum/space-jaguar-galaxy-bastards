function Command(identifier, data) {

  this.identifier = identifier;
  this.data = data;

  this.serialized = function() {
    return "{\"identifier\": " + this.identifier + ", \"data\" : " + this.data + "}";
  };

};
