if (GLOBAL_CONTEXT === undefined) {
  include("com.demensdeum.spaceJaguarGalaxyBastards.includes.js");
  IncludeDependencies();
  GLOBAL_APP_TITLE = "Space Jaguar Galaxy Bastards v0.0.1";
  GLOBAL_CONTEXT = new Context();
  var address = "ws://localhost:8765";
  GLOBAL_CONTEXT.initializeGame = function() {
    var controller = new MainController(address);
    this.switchToController(controller);
  };
}

GLOBAL_CONTEXT.step();
