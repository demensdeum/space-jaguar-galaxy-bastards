function CommandTypeEnum() {
  this.Echo = 0;
  this.EnterTheGame = 1;
  this.SendObject = 2;
  this.GetObjects = 3;
  this.ExitTheGame = 4;
};

var CommandType = new CommandTypeEnum();
