function CallResultCodeEnum() {
  this.Fail = 0;
  this.Success = 1;
  this.SceneObjects = 2;
};

CallResultCode = new CallResultCodeEnum();

function CallResult(dictionary) {
  this.code = dictionary["code"];
  this.description = dictionary["description"];
  this.objects = new List();
  if (this.code == CallResultCode.SceneObjects) {
    var rawObjects = ListFromArray(eval(this.description));
    var self = this;
    rawObjects.forEach(function(item) {
      self.objects.push(eval(item));
    });
  }
}
