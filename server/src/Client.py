import uuid

class Client:
    def __init__(self):
        self.identifier = uuid.uuid4()

    def __hash__(self):
        return hash(self.identifier)

    def __eq__(self, other):
        return self.identifier == other.identifier