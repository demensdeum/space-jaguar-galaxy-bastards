import asyncio
import websockets
import logging
from Client import *
from CallResult import *

class WebSocketsTransportLayer:

    async def clientHandler(self, websocket, path):
        client = Client()
        self.__clientIdentifierToWebsocket[client.identifier] = websocket
        while True:
            data = await websocket.recv()
            response = self.__delegate.handleDataFromClient(data, client)
            if isinstance(response, CallResult):
                response = response.toJson()
            await websocket.send(response)

    def main(self, delegate):
        self.__clientIdentifierToWebsocket = dict()
        self.__delegate = delegate
        self.__start_server = websockets.serve(self.clientHandler, "localhost", 8765)
        asyncio.get_event_loop().run_until_complete(self.__start_server)
        asyncio.get_event_loop().run_forever()