from threading import Lock
from copy import deepcopy
from SceneObject import *
import json

class SceneController:
    def __init__(self):
        self.__lock = Lock()
        self.__objects = dict()

    def setObject(self, object):
        self.__lock.acquire()
        self.__objects[object.name] = object
        self.__lock.release()

    def sceneObjectFromRandomRespawn(self, name):
        return SceneObject(name, 1, 2, 3, 4, 5, 6, 1, 1, 1, {})

    def removeObject(self, object):
        self.__lock.acquire()
        del self.__objects[object.name]
        self.__lock.release()

    def jsonObjects(self):
        self.__lock.acquire()
        jsonObjects = json.dumps(list(map(lambda x: x[1].toJson(), self.__objects.items())))
        self.__lock.release()
        return jsonObjects

    def objects(self):
        self.__lock.acquire()
        objectsCopy = deepcopy(self.__objects)
        self.__lock.release()
        return objectsCopy

    def __mergeDicts(self, lhs, rhs):
        merged = lhs.copy()
        merged.update(rhs)
        return merged

    def applyObjects(self, objects):
        self.__lock.acquire()
        self.__objects = self.__mergeDicts(self.__objects, objects)
        self.__lock.release()