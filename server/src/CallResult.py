import json

Fail = 0
Success = 1
SceneObjects = 2

class CallResult:
    def __init__(self, code, description):
        self.code = code
        self.description = description

    def toJson(self):
        return json.dumps({"code": self.code, "description": self.description})