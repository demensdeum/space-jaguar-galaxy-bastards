import json
import logging, sys
from WebSocketsTransportLayer import *
from MainController import *

logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)

def main():
    transportLayer = WebSocketsTransportLayer()
    mainController = MainController(transportLayer)
    mainController.main()

main()