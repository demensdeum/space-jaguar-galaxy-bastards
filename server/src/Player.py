class Player:

    def __init__(self, name, client, sceneObject):
        self.name = name
        self.client = client
        self.sceneObject = sceneObject

    def __hash__(self):
        return hash(self.name) + hash(self.client)

    def __eq__(self, other):
        return self.name == other.name and self.client.identifier == other.client.identifier