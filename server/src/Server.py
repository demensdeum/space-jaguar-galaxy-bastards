import logging
import json
import sys
from json.decoder import JSONDecodeError

class Server:
    def __init__(self):
        logging.debug(f"{self.__class__} __init__")

    def handleDataFromClient(self, data, client):
        logging.debug(f"{self.__class__}: Handle data {data}, from client {client.identifier}")
        try:
            rawDict = json.loads(data)
        except JSONDecodeError as error:
            logging.debug(f"{error}")
        except:
            logging.error(f"{self.__class__} handleDataFromClient error: {sys.exc_info()[0]}, data: {data}")

        return self.handleRawDictFromClient(rawDict, client)

    def handleRawDictFromClient(self, rawDict, client):
        logging.debug(f"{self.__class__}: Handle json {json}, from client {client.identifier}")
        return self.__delegate.handleRawDictFromClient(rawDict, client)

    def main(self, delegate, transportLayer):
        logging.debug(f"{self.__class__} main")
        self.__delegate = delegate
        self.__transportLayer = transportLayer
        self.__transportLayer.main(self)