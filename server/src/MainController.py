import logging, threading
from Server import *
from SceneController import *
from ServerToSceneControllerAdapter import *
from GameLogicModuleToSceneControllerAdapter import *
from SpaceJaguarGalaxyBastardsGameLogicModule import *

class MainController:
    def __init__(self, transportLayer):
        logging.debug("MainController __init__")
        self.__transportLayer = transportLayer
        self.__server = Server()
        self.__sceneController = SceneController()
        self.__serverToSceneControllerAdapter = ServerToSceneControllerAdapter(self.__server, self.__sceneController)
        self.__gameLogicModuleToSceneControllerAdapter = GameLogicModuleToSceneControllerAdapter(self.__sceneController)
        self.__gameLogicModule = SpaceJaguarGalaxyBastardsGameLogicModule(self.__gameLogicModuleToSceneControllerAdapter, self.__gameLogicModuleToSceneControllerAdapter)

    def gameLogicStep(self):
        isRunning = True
        while isRunning:
            for thread in threading.enumerate():
                if thread.name == "MainThread" and thread.is_alive() == False:
                    isRunning = False
            self.__gameLogicModule.poll()

    def main(self):
        self.__gameLogicThread = threading.Thread(name="Game Logic Thread", target=self.gameLogicStep)
        self.__gameLogicThread.start()
        logging.debug("MainController main")
        self.__server.main(self.__serverToSceneControllerAdapter, self.__transportLayer)