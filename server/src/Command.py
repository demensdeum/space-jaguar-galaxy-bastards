import json

class Command:
    def __init__(self, identifier, data):
        self.identifier = identifier
        self.data = data

    @staticmethod
    def fromRawDict(rawDict):
        return Command(rawDict["identifier"], rawDict["data"])

    def toJson(self):
        return json.dumps({"identifier" : self.identifier, "data" : self.data})
