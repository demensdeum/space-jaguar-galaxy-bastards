from GameLogicModule import *
from SceneObject import *
from math import sin, cos
import logging


class SpaceJaguarGalaxyBastardsGameLogicModule(GameLogicModule):

    def __init__(self, dataSource, delegate, Debug=False):
        super().__init__(dataSource, delegate, Debug)
        self.botsOperator = self.BotsOperator(Debug=self._DEBUG)

    def step(self, objects):
        if self._DEBUG:
            logging.debug("Space Jaguar Galaxy Bastards Game Logic Module step")
        self.botsOperator.step(objects)

    class BotsOperator:

        def __init__(self, Debug=False):
            self.kBotName = "[BOT]"
            self._DEBUG = Debug

        def step(self, objects):
            if self._DEBUG:
                logging.debug(len(objects))
            if self.kBotName in objects:
                sceneObject = objects[self.kBotName]
                x = sceneObject.data["x"]
                x += 0.0001
                y = sceneObject.data["y"]
                y += 0.0001
                sceneObject.data["x"] = x
                sceneObject.data["y"] = y
                sceneObject.x = sin(x)
                sceneObject.y = cos(y)
                if self._DEBUG:
                    logging.debug(sceneObject.x)
                    logging.debug(sceneObject.y)
                objects[self.kBotName] = sceneObject
            else:
                bot = self.Bot(self.kBotName)
                objects[self.kBotName] = bot.sceneObject

        class Bot:
            def __init__(self, name):
                self.sceneObject = SceneObject(name, 1, 2, 3, 4, 5, 6, 1, 1, 1, {"x": 0.0,"y" : 0.0, "z" : 0.0})

