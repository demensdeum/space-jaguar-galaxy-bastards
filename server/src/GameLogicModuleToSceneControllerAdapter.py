class GameLogicModuleToSceneControllerAdapter():
    def __init__(self, sceneController):
        self.__sceneController = sceneController

    def gameLogicModuleDidRequestObjects(self, gameLogicModule):
        return self.__sceneController.objects()

    def gameLogicModuleDidSendObjects(self, gameLogicModule, objects):
        self.__sceneController.applyObjects(objects)