import logging
from Command import *
from CommandType import *
from Player import *
from CallResult import *
from SceneObject import *

class ServerToSceneControllerAdapter:
    def __init__(self, server, sceneController):
        logging.debug(f"{self.__class__} initialized")
        self.__players = set()
        self.__clientToPlayer = dict()
        self.__server = server
        self.__sceneController = sceneController

    def addPlayerToCache(self, player):
        self.__players.add(player)
        self.__clientToPlayer[player.client] = player
        self.__sceneController.setObject(player.sceneObject)

    def removePlayerFromCache(self, player):
        self.__players.remove(player)
        del self.__clientToPlayer[player.client]
        self.__sceneController.removeObject(player.sceneObject)

    def handleRawDictFromClient(self, rawDict, client):
        logging.debug(f"{self.__class__} handle rawDict {rawDict} from client {client.identifier}")
        command = Command.fromRawDict(rawDict)
        logging.debug(f"{self.__class__} command identifier: {command.identifier}")
        if command.identifier == Echo:
            return command.data

        elif command.identifier == EnterTheGame:
            logging.debug(command.data)
            name = command.data["name"]
            sceneObject = self.__sceneController.sceneObjectFromRandomRespawn(name)
            player = Player(name, client, sceneObject)
            logging.debug(player.__hash__())
            if player in self.__players:
                return CallResult(Fail, f"Can't add same client \"{player.client.identifier}\" to game")
            elif any(x.name == player.name for x in self.__players):
                return CallResult(Fail, f"Name \"{player.name}\" already in use")
            else:
                self.addPlayerToCache(player)
                return CallResult(Success, f"Added \"{player.name}\" to game")

        elif command.identifier == SendObject:
            logging.debug(command.data)
            if client in self.__clientToPlayer:
                sceneObject = SceneObject.fromRawDict(command.data)
                clientSceneObject = self.__clientToPlayer[client].sceneObject
                if clientSceneObject.name == sceneObject.name:
                    self.__sceneController.setObject(sceneObject)
                    return CallResult(Success, f"OK")
                else:
                    return CallResult(Fail, f"Sorry, can't change someone-else's object")
            else:
                return CallResult(Fail, f"No scene object for client \"{client.identifier}\"")

        elif command.identifier == GetObjects:
            jsonObjects = self.__sceneController.jsonObjects()
            return CallResult(SceneObjects, jsonObjects)

        elif command.identifier == ExitTheGame:
            if client in self.__clientToPlayer:
                player = self.__clientToPlayer[client]
                self.removePlayerFromCache(player)
                return CallResult(Success, f"Player \"{player.name}\" ejected")
            else:
                return CallResult(Fail, f"Can't eject client \"{client.identifier}\", not in the game")

        else:
            return CallResult(Fail, f"Unknown command identifier: {command.identifier} ")
