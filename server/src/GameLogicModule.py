import logging

class GameLogicModule:

    def __init__(self, dataSource, delegate, Debug=False):
        self.__dataSource = dataSource
        self.__delegate = delegate
        self._DEBUG = Debug

    def poll(self):
        if self._DEBUG:
            logging.debug("Game Logic Module poll")
        objects = self.__dataSource.gameLogicModuleDidRequestObjects(self)
        self.step(objects)
        self.__delegate.gameLogicModuleDidSendObjects(self, objects)

    def step(self, objects):
        raise Exception('GameLogicModule unimplemented')