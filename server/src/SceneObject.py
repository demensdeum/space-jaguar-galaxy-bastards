import json

class SceneObject:

    @staticmethod
    def fromRawDict(rawDict):
        object = SceneObject(
            rawDict["name"],
            rawDict["x"],
            rawDict["y"],
            rawDict["z"],
            rawDict["rX"],
            rawDict["rY"],
            rawDict["rZ"],
            rawDict["sX"],
            rawDict["sY"],
            rawDict["sZ"],
            rawDict["data"]
        )
        return object

    def __init__(self, name, x, y, z, rX, rY, rZ, sX, sY, sZ, data):

        self.name = name

        self.x = x
        self.y = y
        self.z = z

        self.rX = rX
        self.rY = rY
        self.rZ = rZ

        self.sX = sX
        self.sY = sY
        self.sZ = sZ

        self.data = data

    def toJson(self):
        return json.dumps({
            "name": self.name,

            "x": self.x,
            "y": self.y,
            "z": self.z,

            "rX": self.rX,
            "rY": self.rY,
            "rZ": self.rZ,

            "sX": self.sX,
            "sY": self.sY,
            "sZ": self.sZ,

            "data": self.data
        })

    def fillFromRawDict(self, rawDict):
        self.x = rawDict["x"]
        self.y = rawDict["y"]
        self.z = rawDict["z"]

        self.rX = rawDict["rX"]
        self.rY = rawDict["rY"]
        self.rZ = rawDict["rZ"]

        self.sX = rawDict["sX"]
        self.sY = rawDict["sY"]
        self.sZ = rawDict["sZ"]

        self.data = rawDict["data"]