#!/usr/bin/env python

import asyncio
import websockets
from Command import *
from CommandType import *

async def test():
    uri = "ws://localhost:8765"

    async with websockets.connect(uri) as websocket:
        data = {
            "x"  : 1,
            "y"  : 2,
            "z"  : 3,
            "rX" : 4,
            "rY" : 5,
            "rZ" : 6,
            "sX" : 1,
            "sY" : 1,
            "sZ" : 1
        }

        command = Command(SendObject, data)
        await websocket.send(command.toJson())
        response = await websocket.recv()

        print(f"< {response}")

asyncio.get_event_loop().run_until_complete(test())