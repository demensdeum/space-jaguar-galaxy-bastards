#!/usr/bin/env python

import asyncio
import websockets
from Command import *
from CommandType import *

async def test():
    uri = "ws://localhost:8765"

    async with websockets.connect(uri) as websocket:
        data = {}

        command = Command(GetObjects, data)
        await websocket.send(command.toJson())
        response = await websocket.recv()

        print(f"< {response}")

asyncio.get_event_loop().run_until_complete(test())